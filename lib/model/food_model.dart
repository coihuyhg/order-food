class FoodModel {
  String? name;
  double? price;
  String? image;
  String? description;

  FoodModel({
    this.name,
    this.price,
    this.image,
    this.description,
  });
}
