import 'package:flutter/material.dart';
import 'package:order_food/model/food_model.dart';

import '../../widgets/app_bar_widget/app_bar_widget.dart';
import '../home_food/home_food.dart';

class AddFood extends StatefulWidget {
  const AddFood({Key? key, required this.foodList}) : super(key: key);
  final ValueNotifier<List<FoodModel>> foodList;

  @override
  State<AddFood> createState() => _AddFoodState();
}

class _AddFoodState extends State<AddFood> {
  // List foodList = <FoodModel>[];

  final nameController = TextEditingController();
  final priceController = TextEditingController();
  final imageController = TextEditingController();
  final descriptionController = TextEditingController();

  final ValueNotifier<String> nameValueNotifier = ValueNotifier("");
  final priceValueNotifier = ValueNotifier<double>(0.0);
  final imageValueNotifier = ValueNotifier<String>("");
  final descriptionValueNotifier = ValueNotifier<String>("");

  @override
  void dispose() {
    // TODO: implement dispose
    nameController.dispose();
    priceController.dispose();
    imageController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  //Thêm thông tin food
  void addFood() {
    // setState(() {
    //
    //
    //   nameController.clear();
    //   priceController.clear();
    //   imageController.clear();
    //   descriptionController.clear();
    //   nameValueNotifier.value = "";
    //   priceValueNotifier.value = 0.0;
    //   imageValueNotifier.value = "";
    //   descriptionValueNotifier.value = "";
    //   //
    //   // print(name);
    //   // print(price);
    //   // print(image);
    //   // print(description);
    // });
    String name = nameController.text;
    double price = double.parse(priceController.text);
    String image = imageController.text;
    String description = descriptionController.text;
    FoodModel food = FoodModel(
        name: name, price: price, image: image, description: description);
    widget.foodList.value.add(food);
    ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(content: Text('Đã thêm đồ ăn')));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: "Add",
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(Icons.keyboard_arrow_left),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 50, right: 50, top: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text("Name"),
            const SizedBox(height: 6),
            TextFormField(
              controller: nameController,
              onChanged: (value) => nameValueNotifier.value = value,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  hintText: "Enter name"),
            ),
            const SizedBox(height: 20),
            const Text("Price"),
            const SizedBox(height: 6),
            TextFormField(
              controller: priceController,
              onChanged: (value) =>
                  priceValueNotifier.value = double.tryParse(value) ?? 0.0,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  hintText: "Enter price"),
            ),
            const SizedBox(height: 20),
            const Text("Image"),
            const SizedBox(height: 6),
            TextFormField(
              controller: imageController,
              onChanged: (value) => imageValueNotifier.value = value,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  hintText: "Enter image link"),
            ),
            const SizedBox(height: 20),
            const Text("Description"),
            const SizedBox(height: 6),
            TextFormField(
              controller: descriptionController,
              onChanged: (value) => descriptionValueNotifier.value = value,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  hintText: "Description"),
            ),
          ],
        ),
      ),
      bottomSheet: Container(
        width: MediaQuery.of(context).size.width,
        height: 90,
        padding: const EdgeInsets.only(left: 50, right: 50, bottom: 42),
        child: ElevatedButton(
          onPressed: addFood,
          child: const Text("Save"),
        ),
      ),
    );
  }
}
