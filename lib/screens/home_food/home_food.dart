import 'package:flutter/material.dart';
import 'package:order_food/model/food_model.dart';
import 'package:order_food/screens/add_food/add_food.dart';
import 'package:order_food/screens/order_food/order_food.dart';

import '../../widgets/app_bar_widget/app_bar_widget.dart';

class HomeOrderFood extends StatefulWidget {
  final FoodModel? foodModel;

  const HomeOrderFood({Key? key, this.foodModel}) : super(key: key);

  @override
  State<HomeOrderFood> createState() => _HomeOrderFoodState();
}

class _HomeOrderFoodState extends State<HomeOrderFood> {
  ValueNotifier<List<FoodModel>> foodList = ValueNotifier([]);

  @override
  void initState() {
    // TODO: implement initState
    // foodList.addListener(() {
    //   print(foodList.value.toString());
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(foodList.value);
    return Scaffold(
      appBar: AppBarWidget(title: "Home"),
      body: ListView.builder(
        itemCount: foodList.value.length,
        itemBuilder: (context, index) {
          FoodModel food = foodList.value[index];
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 18, bottom: 21),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      height: 100,
                      width: 100,
                      child: Image.network("${food.image}"),
                    ),
                    const SizedBox(width: 33),
                    Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${food.name}",
                            style: const TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 14),
                          Text(
                            "${food.price}",
                            style: const TextStyle(
                                fontSize: 16, color: Color(0xFFFF1F00)),
                          ),
                        ],
                      ),
                    ),
                    IconButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const OrderFood()));
                        },
                        icon: const Icon(Icons.shopping_cart,
                            color: Color(0xFF00A3FF))),
                  ],
                ),
              ),
              const Padding(
                  padding: EdgeInsets.only(left: 12, right: 12),
                  child: Divider(
                    thickness: 2,
                  ))
            ],
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          foodList.value.add(FoodModel(name: "hu",
              price: 5,
              image: "https://media.istockphoto.com/id/175197961/photo/salad-plate.jpg?s=612x612&w=0&k=20&c=9lxiIPsTBH7ytCXWuC__FvWyxkPxtkJ2evoNLrwhOVA=",
              description: "Ngon"));
          // Navigator.push(
          //     context,
          //     MaterialPageRoute(
          //         builder: (context) => AddFood(
          //               foodList: foodList,
          //             )));
        },
        tooltip: "Order",
        child: const Icon(Icons.add),
      ),
    );
  }
}
