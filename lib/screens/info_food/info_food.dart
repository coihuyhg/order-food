import 'package:flutter/material.dart';
import 'package:order_food/widgets/app_bar_widget/app_bar_widget.dart';

class InfoFood extends StatefulWidget {
  const InfoFood({Key? key}) : super(key: key);

  @override
  State<InfoFood> createState() => _InfoFoodState();
}

class _InfoFoodState extends State<InfoFood> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: "Salad",
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(Icons.keyboard_arrow_left),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 18, right: 18, top: 18),
        child: Column(
          children: [
            Container(
              height: 200,
              width: MediaQuery.of(context).size.width,
              color: Colors.red,
            ),
            const SizedBox(height: 21),
            const Text(
                "A salad is a dish consisting of mixed, mostly natural ingredients. They are typically served at room temperature or chilled, though some can be served warm"),
            const SizedBox(height: 23),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text("Price", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("\$3"),
              ],
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text("Quantity", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("5"),
              ],
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text("Total", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("\$15"),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
