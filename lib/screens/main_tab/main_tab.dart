import 'package:flutter/material.dart';

import '../home_food/home_food.dart';
import '../order_food/order_food.dart';

class MainTab extends StatefulWidget {
  const MainTab({Key? key}) : super(key: key);

  @override
  State<MainTab> createState() => _MainTabState();
}

class _MainTabState extends State<MainTab> {
  int selected = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        bottomNavigationBar: menu(),
        body: const TabBarView(
          children: [HomeOrderFood(), OrderFood()],
        ),
      ),
    );
  }

  Widget menu() {
    return TabBar(
      onTap: (index) {
        setState(() {
          selected = index;
        });
      },
      labelStyle: const TextStyle(fontSize: 12),
      unselectedLabelStyle: const TextStyle(fontSize: 12),
      indicatorColor: Colors.transparent,
      labelColor: const Color(0xFF00A3FF),
      unselectedLabelColor: const Color(0xFF6B6B6B),
      tabs: [
        Tab(
            text: "Home",
            icon: selected == 0
                ? Image.asset("assets/icons/ic_home_selected.png")
                : Image.asset("assets/icons/ic_home_unselected.png")),
        Tab(
            text: "Order",
            icon: selected == 1
                ? Image.asset("assets/icons/ic_order_selected.png")
                : Image.asset("assets/icons/ic_order_unselected.png")),
      ],
    );
  }
}
