import 'package:flutter/material.dart';
import 'package:order_food/screens/info_food/info_food.dart';
import 'package:order_food/widgets/app_bar_widget/app_bar_widget.dart';

class OrderFood extends StatefulWidget {
  const OrderFood({Key? key}) : super(key: key);

  @override
  State<OrderFood> createState() => _OrderFoodState();
}

class _OrderFoodState extends State<OrderFood> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(title: "order"),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: 2,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, top: 18, bottom: 21),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            color: Colors.red,
                          ),
                          const SizedBox(width: 33),
                          Expanded(
                            flex: 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text(
                                  "Salad",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "\$3",
                                  style: TextStyle(
                                      fontSize: 16, color: Color(0xFFFF1F00)),
                                ),
                                Text(
                                  "5",
                                  style: TextStyle(fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => const InfoFood(),
                                    ));
                              },
                              icon: const Icon(Icons.keyboard_arrow_right,
                                  color: Color(0xFF6B6B6B))),
                        ],
                      ),
                    ),
                    const Padding(
                        padding: EdgeInsets.only(left: 12, right: 12),
                        child: Divider())
                  ],
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 18, right: 18, bottom: 34),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [Text("Quantity"), Text("7")],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [Text("Total"), Text("\$21")],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
