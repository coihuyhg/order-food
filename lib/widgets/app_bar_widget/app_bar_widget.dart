import 'package:flutter/material.dart';

class AppBarWidget extends AppBar {
  AppBarWidget({Key? key, String? title, Widget? leading})
      : super(
          key: key,
          title: Text("$title".toUpperCase(),
              style: const TextStyle(fontSize: 18)),
          centerTitle: true,
          backgroundColor: const Color(0xFF00A3FF),
          leading: leading,
        );
}